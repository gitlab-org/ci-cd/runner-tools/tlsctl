package main

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "tlsctl"
	app.Usage = "Set of commands to debug TLS certificates for GitLab Runner."
	app.Description = "Set of commands to debug TLS certificates for GitLab Runner."
	app.Version = Version().ShortLine()
	app.Author = "GitLab Inc. <support@gitlab.com>"

	cli.VersionPrinter = func(_ *cli.Context) {
		fmt.Print(Version().Extended())
	}

	app.Commands = []cli.Command{
		makeSaveCMD(),
		makeInfoCMD(),
		makeChainCMD(),
	}

	err := app.Run(os.Args)
	if err != nil {
		logrus.WithError(err).Fatal("Failed to run command")
	}
}
